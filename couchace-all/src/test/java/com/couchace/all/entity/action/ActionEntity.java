/*
 * Copyright 2012 Harlan Noonkester
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.couchace.all.entity.action;

import com.couchace.annotations.CouchEntity;
import com.couchace.annotations.CouchEntityId;
import com.couchace.annotations.CouchRevision;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonTypeIdResolver;

@JsonTypeIdResolver(ActionEntityResolver.class)
@JsonTypeInfo(use= JsonTypeInfo.Id.CUSTOM, property="actionType")
@CouchEntity("Action")
public interface ActionEntity {

    ActionType getActionType();

    @CouchEntityId("Action:%s")
    String getId();

    @CouchRevision
    String getRevision();

    String getDescription();

}
